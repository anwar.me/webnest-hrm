package com.teamblackhole.realtimechat.webnest_hrm;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

public class AddNoteActivity extends FragmentActivity {

    public static final String EXTRA_TITLE = "com.anwar.dipa.EXTRA_TITLE";
    public static final String EXTRA_DESCRIPTION = "com.anwar.dipa.EXTRA_DESCRIPTION";
    public static final String EXTRA_PRIORITY = "com.anwar.dipa.EXTRA_PRIORITY";

    private EditText title;
    private EditText description;
    private TextView priority;
    private NumberPicker priorityPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        title = findViewById(R.id.edit_text_title);
        description = findViewById(R.id.edit_text_description);

        priorityPicker = findViewById(R.id.number_picker_priority);

        priorityPicker.setMinValue(1);
        priorityPicker.setMaxValue(10);

        setTitle("Add Note");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_note, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_note:
                saveNote();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveNote() {
        String title = this.title.getText().toString();
        String description = this.description.getText().toString();
        int priority = this.priorityPicker.getValue();

        if (title.trim().isEmpty() || description.trim().isEmpty()) {
            Toast.makeText(AddNoteActivity.this, "Empty Field(s)", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = getIntent();
        data.putExtra(EXTRA_TITLE, title);
        data.putExtra(EXTRA_DESCRIPTION, description);
        data.putExtra(EXTRA_PRIORITY, priority);

        setResult(RESULT_OK, data);
        finish();

    }
}
